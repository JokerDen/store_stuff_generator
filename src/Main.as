package {

import com.adobe.images.PNGEncoder;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Loader;
import flash.display.LoaderInfo;
import flash.display.MovieClip;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.FileListEvent;
import flash.events.MouseEvent;
import flash.filesystem.File;
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;
import flash.geom.Matrix;
import flash.geom.Rectangle;
import flash.net.FileFilter;
import flash.net.URLRequest;
import flash.utils.ByteArray;

[SWF(width="640", height="480", frameRate="60", backgroundColor="#FFFFFF")]
public class Main extends Sprite {

    [Embed(source="../res/swf/store_stuff_generator.swf", symbol="main_view")]
    public var MainView:Class;

    private var images:Vector.<BitmapData> = new <BitmapData>[];

    private var configs:Vector.<ScreenConfig> = new <ScreenConfig>[];

    private var addFilesButton:MovieClip;
    private var generateButton:MovieClip;

    public function Main() {
        var view:Sprite = new MainView();
        addChild(view);

        addFilesButton = view["add_files_button"];
        generateButton = view["generate_button"];

        addFilesButton.buttonMode = true;
        addFilesButton.addEventListener(MouseEvent.CLICK, pickFiles);

        generateButton.buttonMode = true;
        generateButton.addEventListener(MouseEvent.CLICK, selectPathToSave);

        setGenerateEnabled(false);

//        addScreenConfig("ios/iphone_5.8/", 2436, 1125);
        addScreenConfig("ios/iphone_5.5/", 2208, 1242);
        addScreenConfig("ios/iphone_4.7/", 1334, 750);
        addScreenConfig("ios/iphone_4/", 1136, 640);
        addScreenConfig("ios/iphone_3.5/", 960, 640);

        addScreenConfig("ios/ipad_12.9/", 2732, 2048);
        addScreenConfig("ios/ipad_9.7/", 2048, 1536);

        addScreenConfig("android/feature/", 1024, 500);
        addScreenConfig("android/promo/", 180, 120);
    }

    private function addScreenConfig(path:String, width:Number, height:Number):void {
        configs.push(new ScreenConfig(path, width, height));
    }

    private function selectPathToSave(...args):void {
        var file:File = File.documentsDirectory;
        file.browseForDirectory("Select folder to save");
        file.addEventListener(Event.SELECT, generate);
    }

    private function generate(event:Event):void {
        var directory:File = event.target as File;

        for (var i:int = 0; i < configs.length; i++) {
            var config:ScreenConfig = configs[i];
            for (var j:int = 0; j < images.length; j++) {
                var image:BitmapData = images[j];

                var scale:Number = config.width / image.width;

                var invertScale:Number = image.width / config.width;
                var rectHeight:Number = config.height * invertScale;
                var rectOffset:Number = (image.height - rectHeight) / 2;
                var clipRect:Rectangle = new Rectangle(0, 0, image.width, image.height - (rectOffset * 2));

                var resizeMatrix:Matrix = new Matrix();
                resizeMatrix.scale(scale, scale);
                resizeMatrix.ty = -rectOffset * scale;

                var resizedImage:BitmapData = new BitmapData(config.width, config.height, false, 0xFF0000);
                resizedImage.draw(image, resizeMatrix, null, null, clipRect, true);

                var pngBA:ByteArray = PNGEncoder.encode(resizedImage);
                var fileName:String = directory.url + "/" + config.path + "screen_" + j + ".png";

                var imageFile:File = new File(fileName);
                var fileStream:FileStream = new FileStream();
                fileStream.open(imageFile, FileMode.WRITE);
                fileStream.writeBytes(pngBA);
                fileStream.close();
                trace("File saved: " + fileName);
            }
        }
        trace("COMPLETED!");
    }

    private function pickFiles(...args):void {
        var file:File = File.documentsDirectory;

        var imageFilter:FileFilter = new FileFilter("Images", "*.png");

        file.browseForOpenMultiple("Select screenshots", [imageFilter]);
        file.addEventListener(FileListEvent.SELECT_MULTIPLE, handleFilesSelect);
    }

    private var pickedFiles:Vector.<File> = new <File>[];

    private function handleFilesSelect(event:FileListEvent):void {
        images.length = 0;
        pickedFiles.length = 0;

        setGenerateEnabled(false);

        for (var i:uint = 0; i < event.files.length; i++) {
            var file:File = event.files[i];
            pickedFiles.push(file);
        }

        parseNextFile();
    }

    private function parseNextFile():void {
        if (!pickedFiles.length && images.length) {
            setGenerateEnabled(true);
        }

        if (pickedFiles.length) {
            var file:File = pickedFiles.shift();
            var loader:Loader = new Loader();
            loader.contentLoaderInfo.addEventListener(Event.COMPLETE, addImage);
            loader.load(new URLRequest(file.url));
        }
    }

    private function addImage(event:Event):void {
        var bitmapData:BitmapData = Bitmap(LoaderInfo(event.target).content).bitmapData;
        images.push(bitmapData);
        parseNextFile();
    }

    private function setGenerateEnabled(value:Boolean):void {
        if (value) {
            generateButton.alpha = 1;
            generateButton.mouseChildren = true;
            generateButton.mouseEnabled = true;
        } else {
            generateButton.alpha = 0.5;
            generateButton.mouseChildren = false;
            generateButton.mouseEnabled = false;
        }
    }
}
}
