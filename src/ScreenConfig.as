package {
public class ScreenConfig {
    private var _path:String;
    private var _width:Number;
    private var _height:Number;

    public function ScreenConfig(path:String, width:Number, height:Number) {
        _path = path;

        _width = width;
        _height = height;
    }

    public function get path():String {
        return _path;
    }

    public function get width():Number {
        return _width;
    }

    public function get height():Number {
        return _height;
    }
}
}
